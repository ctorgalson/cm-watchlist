# CM Watchlist

##  Fields

### `field_cm_watchlist_ticker_symbol`

* max length set to 6 characters; [according to Wikipedia, ticker symbols
  are "typically between 1 and 4
  letters"](http://www.wikiwand.com/en/Ticker_symbol)
