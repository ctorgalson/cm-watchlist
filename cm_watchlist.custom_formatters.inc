<?php
/**
 * @file
 * cm_watchlist.custom_formatters.inc
 */

/**
 * Implements hook_custom_formatters_defaults().
 */
function cm_watchlist_custom_formatters_defaults() {
  $export = array();

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'cm_watchlist_ticker_symbol_formatter';
  $formatter->label = 'CM Watchlist ticker symbol formatter';
  $formatter->description = 'Transforms ticker symbols to links.';
  $formatter->mode = 'php';
  $formatter->field_types = 'text';
  $formatter->code = 'return theme(\'cm_watchlist_ticker_display\', array(\'link_base\' => variable_get(CM_WATCHLIST_BASE_URL_VAR, CM_WATCHLIST_BASE_URL), \'link_parameters\' => _cm_watchlist_get_tickers($object)));';
  $formatter->fapi = '';
  $export['cm_watchlist_ticker_symbol_formatter'] = $formatter;

  return $export;
}
