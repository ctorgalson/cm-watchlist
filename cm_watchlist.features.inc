<?php
/**
 * @file
 * cm_watchlist.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cm_watchlist_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "custom_formatters" && $api == "custom_formatters") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function cm_watchlist_node_info() {
  $items = array(
    'watchlist' => array(
      'name' => t('Watchlist'),
      'base' => 'node_content',
      'description' => t('Use this content type to create a a collection of stocks to monitor.'),
      'has_title' => '1',
      'title_label' => t('Watchlist name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
