function runTicker() {
    "use strict";
    jQuery.getJSON(Drupal.settings.cmWatchlist.ajaxUrl, function(data) {
      var $tickerLink = jQuery('#cm_watchlist_ticker'),
          items = [];
      jQuery.each(data['cm-watchlist-tickers'][0], function(i, e) {
        items.push('<li id="cm-watchlist-' + i + '">' + e + '</li>');
        // Left in place to demonstrate updates happen...remove in production...
        console.log('Updating...');
      });
      $tickerLink.replaceWith(
        jQuery('<ul/>', {
          id: 'cm_watchlist_ticker',
          html: items.join('')
        })
      );
    });
  }


(function ($) {
  /**
   *
   */
  "use strict";
  Drupal.behaviors.cm_watchlist = {
    attach: function () {
      var interval = Drupal.settings.cmWatchlist.updateFrequency * 1000;
      if (parseInt(Drupal.settings.cmWatchlist.updateFrequency) > 0) {
        setInterval('runTicker()', interval);
      }
      else {
        runTicker();
      }
    }
  };
})(jQuery);

