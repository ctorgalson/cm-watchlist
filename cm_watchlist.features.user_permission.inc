<?php
/**
 * @file
 * cm_watchlist.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cm_watchlist_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create watchlist content'.
  $permissions['create watchlist content'] = array(
    'name' => 'create watchlist content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own watchlist content'.
  $permissions['delete own watchlist content'] = array(
    'name' => 'delete own watchlist content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own watchlist content'.
  $permissions['edit own watchlist content'] = array(
    'name' => 'edit own watchlist content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
