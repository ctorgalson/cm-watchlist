<?php
/**
 * @file
 * cm_watchlist.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function cm_watchlist_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-watchlist-body'
  $field_instances['node-watchlist-body'] = array(
    'bundle' => 'watchlist',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Watchlist description',
    'required' => 1,
    'settings' => array(
      'display_summary' => 1,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => -4,
    ),
  );

  // Exported field_instance: 'node-watchlist-field_cm_watchlist_ticker_symbol'
  $field_instances['node-watchlist-field_cm_watchlist_ticker_symbol'] = array(
    'bundle' => 'watchlist',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Enter the stock symbol in this field.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'custom_formatters',
        'settings' => array(),
        'type' => 'custom_formatters_cm_watchlist_ticker_symbol_formatter',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cm_watchlist_ticker_symbol',
    'label' => 'Ticker symbol',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 4,
      ),
      'type' => 'text_textfield',
      'weight' => -3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Enter the stock symbol in this field.');
  t('Ticker symbol');
  t('Watchlist description');

  return $field_instances;
}
